/**
 * 404 (Not Found) Response
 */
module.exports = function(response) {
  const res = this.res;

  if (typeof response === 'string' || typeof response === 'undefined') {
    return res.negotiate(new ErrorHandler('notFound', 'route_not_found'));
  }

  return res.negotiate(response);
};
